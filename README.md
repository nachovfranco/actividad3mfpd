# Actividad 3 del máster de formación de profesorado

En este repositorio se encuentra un notebook interactivo de Jupyter diseñado para ayudar en la enseñanza
del **tiro parabólico** en 1º de Bachillerato, en la asignatura de Física y Química.

Para probarlo, entra en [Binder](http://mybinder.org/), pega esta dirección: (https://bitbucket.org/nachovfranco/actividad3mfpd) y ¡empieza a experimentar!
